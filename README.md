#### Hi there! 

#### I'm Jeri, former product design engineer, now a software engineer. 🥳

**🛠️ Infrastructure & Tools:** Google Kubernetes Engine | Google Compute Engine | Google  
Cloud Pub/Sub | Docker | Ansible | Kibana | Grafana | Linux  
**📁 Databases:** MongoDB | Neo4J | Cassandra | ElasticSearch | Redis  
**📚 Frameworks & Libraries:** Node.js | NestJS | Angular | React | Jest | Bull   
**🗣️ Languages:** JavaScript | TypeScript  

Looking to hire me? Here are my:
- [resumé](https://jerixmx.gitlab.io/resume/)  
- [some old pull requests for a Hatchways co-op program](https://github.com/hatchways/team-old-fashioned/issues?page=1&q=assignee%3Ajerixmx+is%3Aclosed)  
- [some old code reviews for Hatchways](https://github.com/jerixmx?tab=overview&from=2021-10-01&to=2021-10-31)  
- [LinkedIn](https://www.linkedin.com/in/jerimedrano/)  
---


📖 Currently Reading: [A Penguin History of the World](https://www.amazon.com/Penguin-History-World-Sixth/dp/1846144434)  
🏃 Current Track: **Project Monikuh (Productivity App)**  
☑️ Last Completed: [Data Structures and Algorithms in JavaScript - Full Course for Beginners](https://www.youtube.com/watch?v=t2CEgPsws3U) ([complete list](https://gitlab.com/jerixmx/jerixmx/-/snippets/2420276))   
